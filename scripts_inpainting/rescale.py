from PIL import Image
from tqdm import tqdm
import os
import glob
import json


def folder2index(folder_path):
    return int(folder_path.split('/')[-1])


inpainting_images_root = "/home/dani/repos/content_replication/DeepfakeArt/similar/inpainting"
for folder_path in tqdm(sorted(glob.glob(os.path.join(inpainting_images_root, "*")), key=lambda x: folder2index(x))):
    idx = folder2index(folder_path)
    if os.path.exists(os.path.join(folder_path, "group.jpg")):
        old_path = os.path.join(folder_path, "group.jpg")
        new_path = os.path.join(folder_path, "group.png")
        os.system(f"mv {old_path} {new_path}")
    else:
        assert os.path.exists(os.path.join(folder_path, "group.png"))
    for postfix in ["original", "inpainting", "mask"]:
        jpg_path = os.path.join(folder_path, postfix+".jpg")
        png_path = os.path.join(folder_path, postfix+".png")
        if os.path.exists(jpg_path) and os.path.exists(png_path):
            jpg_size = Image.open(jpg_path).size
            png_size = Image.open(png_path).size
            assert abs(round(jpg_size[0] / 1.2) - png_size[0]) <= 1, f"{round(jpg_size[0] / 1.2)=}, {png_size[0]=}"
            assert abs(round(jpg_size[1] / 1.2) - png_size[1]) <= 1, f"{round(jpg_size[1] / 1.2)=}, {png_size[1]=}"
            os.system(f"rm {jpg_path}")
        elif os.path.exists(jpg_path) and not os.path.exists(png_path):
            jpg_size = Image.open(jpg_path).size
            Image.open(jpg_path).resize((int(jpg_size[0]/1.2), int(jpg_size[1]/1.2))).save(png_path)
            os.system(f"rm {jpg_path}")
        elif not os.path.exists(jpg_path) and os.path.exists(png_path):
            pass
        else:
            assert False, f"Neither of jpg or png files found in {folder_path}."
