<img src="https://www.googleapis.com/download/storage/v1/b/kaggle-user-content/o/inbox%2F8198772%2Fe6bd3b238c7a8fee42c62b434bf944e5%2FDeepfakeArt_logo_scaled.png?generation=1683518568593069&alt=media" alt="DeepfakeArt Logo">

# Content Replication <!-- omit in toc -->

Project members: Alexander Wong, Hossein Aboutalebi, Dayou Mao, Carol Xu.

## Table of Contents <!-- omit in toc -->

- [1. DeepfakeArt Version 1.0](#1-deepfakeart-version-10)
  - [1.1. Cutmix](#11-cutmix)
  - [1.2. Segmix](#12-segmix)
  - [1.3. Style Transfer](#13-style-transfer)
  - [1.4. Inpainting](#14-inpainting)
- [2. Future Improvements](#2-future-improvements)
  - [2.1. For better quality](#21-for-better-quality)
  - [2.2. Implementation-wise](#22-implementation-wise)
  - [2.3. Problems with ControlNet generation](#23-problems-with-controlnet-generation)
- [3. Useful commands](#3-useful-commands)

## 1. DeepfakeArt Version 1.0

### 1.1. Cutmix

Images in this section were generated by copying the pixels in a square patch from a source image and overlaying onto a target image. The resulting image is called a "Cutmix" of the source and target image. Both the source image and the target image were chosen randomly from the WikiArt dataset. The patch size and location from the source image and the overlay location onto the target image are all chosen at random. There are 3,000 (source, target, mixed) triplets in this section.

### 1.2. Segmix

Images in this section were generated by copying the pixels corresponding to an instance of object and overlaying onto a target image. The resulting image is called a "Segmix" of the source and target image. Both the source image and the target image were chosen randomly from the PASCAL VOC dataset. Masks of the objects in the source image are labelled by the instance segmentation masks provided by the PASCAL VOC dataset. The instance of object from the source image, resizing of the chosen object, and the overlaying location onto the target image, are all chosen at random. There are 3,000 (source, target, mask, mixed) 4-tuples in this section.

### 1.3. Style Transfer

Images in this section were generated by passing images in the WikiArt dataset to the ControlNet model. The generations were guided by Canny edge detections and prompts. 200 images were selected from each of the sub-directories of the WikiArt dataset, with the exception of Action_painting and Analytical_Cubism, in which there are only 98 and 110 images, respectively. There were a total of 4 different prompts used: "a high-quality, detailed, realistic image", "a high-quality, detailed, cartoon style drawing", "a high-quality, detailed, oil painting", and "a high-quality, detailed, pencil drawing". Each prompt was used for a quarter of the images from each sub-directory. Finally, generated outputs of poor quality were discarded, resulting in a total of 3,213 (original, edges, prompt, generated) 4-tuples in this section.

Good ones:
Buildings:
  Naive_Art_Primitivism: 117, 127
Single person:
  Northern_Renaissance: 73, 84, 90, 106, 122, 128, 159
A few people:
  New_Realism: 21, 46
Country scene:
  New_Realism: 137
Single object:
  Northern_Renaissance: 118, 173

### 1.4. Inpainting

<img src="https://www.googleapis.com/download/storage/v1/b/kaggle-user-content/o/inbox%2F8198772%2F80475ca36202cfa7732e860c818c8240%2Finpainting1.png?generation=1683518884067981&alt=media"
    alt="Inpainting Example 1"
    width="50%" height="50%"><img src="https://www.googleapis.com/download/storage/v1/b/kaggle-user-content/o/inbox%2F8198772%2F6416e52717ef0154328a2f59222285c8%2Finpainting2.png?generation=1683518887661924&alt=media"
    alt="Inpainting Example 2"
    width="50%" height="50%">

<img src="https://www.googleapis.com/download/storage/v1/b/kaggle-user-content/o/inbox%2F8198772%2Fc6a75fb96ae69187584770addf8106ae%2Finpainting3.png?generation=1683518890664496&alt=media"
    alt="Inpainting Example 3"
    width="50%" height="50%"><img src="https://www.googleapis.com/download/storage/v1/b/kaggle-user-content/o/inbox%2F8198772%2F00ee5b84f1229efc582d9dda904ec4d3%2Finpainting4.png?generation=1683518893449388&alt=media"
    alt="Inpainting Example 4"
    width="50%" height="50%">

## 2. Future Improvements

### 2.1. For better quality

* increase resolution of Canny edge detection.
* use multi-scale Canny edge detection.
* stack Canny edges with other conditions like depth map.
* use source image as condition.
* fine-tune stable diffusion on wikiart.
* somehow separate the generation of geometry and the filling of color.
* canny edges contain no semantic information. maybe somehow insert some semantics as condition to guide the diffusion.
* is the diffusion model trained with different conditions (Canny edges, depth map, etc.) all together? does the diffusion model need to specialize with one of the conditions.
* diffusion models are using local information only?? some how increase the range of dependencies for global semantics. seems like a 512 resolution is easier to get the semantics right than the 1024 resolution. this might be related to the receptive field size. as the diffusion model is CNN-based.

### 2.2. Implementation-wise

* try some pretrained nudity detection model.
* preprocess for duplicate images to save some computation during generation. maybe add this step to the dataset constructor.
* save and push configs to git for reproducibility check and use deterministic algo to generate images from the configs. The `Cutmix` and `Segmix` sections are done this way. Others need to be refactored and reproduced.

### 2.3. Problems with ControlNet generation

* the model is sometimes adding a dust cloud to the background while the background of the source image is clean, when operating at high resolution (1024).
* the model is sometimes generating a black-and-white image at a higher resolution (1024) while generating a colorful image at a lower resolution (512).

## 3. Useful commands

```bash
watch -n 0.5 nvidia-smi --query-gpu=index,fan.speed,temperature.gpu,memory.used,memory.total --format=csv
```
