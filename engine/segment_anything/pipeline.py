import numpy
import os
import matplotlib.pyplot as plt
import sys
import cv2

from engine.segment_anything.segment_anything import sam_model_registry, SamAutomaticMaskGenerator, SamPredictor


class SAM:

    CHECKPOINT_URL = "https://dl.fbaipublicfiles.com/segment_anything/sam_vit_h_4b8939.pth"
    CHECKPOINT_PATH = "/home/dani/repos/content_replication/engine/segment_anything/checkpoints/sam_vit_h_4b8939.pth"
    MODEL_TYPE = "vit_h"
    LOW_AREA_PERCENTAGE_THRESHOLD = 0.05

    def __init__(self, download=False, device="cuda:0"):
        if download:
            os.system(f"wget -O {self.CHECKPOINT_PATH} {self.CHECKPOINT_URL}")
        sam_model = sam_model_registry[self.MODEL_TYPE](checkpoint=self.CHECKPOINT_PATH)
        sam_model.to(device)
        self.generator = SamAutomaticMaskGenerator(
            model=sam_model,
            points_per_side=32,
            pred_iou_thresh=0.86,
            stability_score_thresh=0.92,
            crop_n_layers=1,
            crop_n_points_downscale_factor=2,
            min_mask_region_area=64*64,  # Requires open-cv to run post-processing
        )

    def __call__(self, img_path):
        image = cv2.imread(img_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        assert type(image) == numpy.ndarray and len(image.shape) == 3 and image.shape[2] == 3
        low_pixels_threshold = self.LOW_AREA_PERCENTAGE_THRESHOLD * image.shape[0] * image.shape[1]
        mask_list = self.generator.generate(image)
        final_mask = numpy.zeros(shape=(image.shape[0], image.shape[1]))
        clsidx = 1
        small_mask = numpy.zeros(shape=(image.shape[0], image.shape[1]))
        for mask in mask_list:
            area = mask['area']
            mask = erode(mask['segmentation'])
            assert type(mask) == numpy.ndarray and len(mask.shape) == 2 and mask.shape[:] == image.shape[:2]
            if area > low_pixels_threshold:
                final_mask = numpy.where(mask, clsidx, final_mask)
                clsidx += 1
            else:
                small_mask = numpy.logical_or(small_mask, mask)
        final_mask = numpy.where(small_mask, clsidx, final_mask)
        final_mask = final_mask.astype(numpy.uint8)
        return final_mask


def erode(mask):
    assert set(numpy.unique(mask)).issubset(set([0, 1]))
    kernel_size = 3
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size, kernel_size))
    mask = (mask * 255).astype(numpy.float32)
    mask = cv2.erode(mask, kernel, iterations=1)
    _, mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)
    mask = mask == 255
    return mask
