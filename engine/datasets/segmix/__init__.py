"""
SEGMIX API.
"""
from engine.datasets.segmix.SegmixGen import SegmixGen


__all__ = (
    "SegmixGen",
)
