import torch
import torchvision
import os
import random
from tqdm import tqdm
import json


class SegmixGen:

    MIN_RESIZE = 0.5
    MAX_RESIZE = 0.9

    def __init__(self, source, config_path, images_dir):
        self.source = source
        self.config_path = config_path
        self.images_dir = images_dir
        if not os.path.exists(self.images_dir):
            raise ValueError(f"[ERROR] Images directory path \"{self.images_dir}\" not found.")

    def generate_config(self, num_examples):
        print("Generating config...")
        assert self.source is not None
        random.seed(0)
        config_dict = {}
        for idx in tqdm(range(num_examples), leave=False):
            example_dict = {}
            example_dict['s_idx'], example_dict['t_idx'] = random.sample(range(len(self.source)), k=2)
            example_dict['s_size'] = list(self.source[example_dict['s_idx']][0].shape[-2:])
            example_dict['t_size'] = list(self.source[example_dict['t_idx']][0].shape[-2:])
            example_dict['obj'] = random.choice(list(set(torch.unique(self.source[example_dict['s_idx']][1]).tolist()) - set([0])))  # 0 is background
            th, tw = example_dict['t_size']
            mh = int(th * random.uniform(self.MIN_RESIZE, self.MAX_RESIZE))
            mw = int(tw * random.uniform(self.MIN_RESIZE, self.MAX_RESIZE))
            example_dict['m_size'] = [mh, mw]
            mi = random.randint(0, th-1-mh)
            mj = random.randint(0, tw-1-mw)
            example_dict['m_loc'] = [mi, mj]
            # data augmentation
            example_dict['flip'] = random.choice([0, 1])
            # add to config_dict
            config_dict[idx] = example_dict
        with open(self.config_path, mode='w') as f:
            json.dump(obj=config_dict, fp=f, indent=4)
        print("Done.")

    def generate_images(self):
        """
        Data augmentations were not used.
        """
        print("Generating images...")
        annotations = {}
        if not os.path.exists(self.config_path):
            raise ValueError(f"[ERROR] Config file not found.")
        with open(self.config_path, mode='r') as f:
            config_dict = json.load(f)
        for idx, example_dict in tqdm(config_dict.items(), leave=False):
            idx = int(idx)
            s_image, s_label = self.source[example_dict['s_idx']]
            s_label = torch.unsqueeze(s_label, dim=0)
            assert len(s_label.shape) == 3, f"{s_label.shape=}"
            assert s_label.dtype == torch.int64
            mask = (s_label == example_dict['obj']).type(torch.int64)
            i_range = (torch.sum(mask, dim=-1).squeeze() != 0)
            j_range = (torch.sum(mask, dim=-2).squeeze() != 0)
            s_image = s_image[:, i_range, :][:, :, j_range]
            mask = mask[:, i_range, :][:, :, j_range]
            assert s_image.shape[-2:] == mask.shape[-2:] == (torch.sum(i_range), torch.sum(j_range)), f"{s_image.shape=}, {torch.sum(i_range)=}, {torch.sum(j_range)=}"
            t_image = self.source[example_dict['t_idx']][0]
            # transform
            s_image = torchvision.transforms.Resize(
                size=example_dict['m_size'], interpolation=torchvision.transforms.functional.InterpolationMode.BILINEAR,
            )(s_image)
            mask = torchvision.transforms.Resize(
                size=example_dict['m_size'], interpolation=torchvision.transforms.functional.InterpolationMode.NEAREST,
            )(mask)
            if example_dict['flip'] == 1:
                torchvision.transforms.functional.hflip(s_image)
                torchvision.transforms.functional.hflip(mask)
            # padding
            mi, mj = example_dict['m_loc']
            mh, mw = example_dict['m_size']
            th, tw = example_dict['t_size']
            pad = [mj, tw-mj-mw, mi, th-mi-mh]
            s_image = torch.nn.functional.pad(s_image, pad=pad, mode='constant', value=0)
            mask = torch.nn.functional.pad(mask, pad=pad, mode='constant', value=0)
            assert s_image.shape[-2:] == mask.shape[-2:] == t_image.shape[-2:]
            # create new image
            new_image = mask * s_image + (1-mask) * t_image
            torchvision.utils.save_image(s_image, os.path.join(self.images_dir, f"{idx:03d}_source_1.png"))
            torchvision.utils.save_image(t_image, os.path.join(self.images_dir, f"{idx:03d}_source_2.png"))
            torchvision.utils.save_image(mask.type(torch.float32), os.path.join(self.images_dir, f"{idx:03d}_mask.png"))
            torchvision.utils.save_image(new_image, os.path.join(self.images_dir, f"{idx:03d}_generated.png"))
            # save annotations
            example_dict = {}
            example_dict['source_1'] = os.path.join(self.images_dir, f"{idx:03d}_source_1.png")
            example_dict['source_2'] = os.path.join(self.images_dir, f"{idx:03d}_source_2.png")
            example_dict['mask'] = os.path.join(self.images_dir, f"{idx:03d}_mask.png")
            example_dict['generated'] = os.path.join(self.images_dir, f"{idx:03d}_generated.png")
            annotations[idx] = example_dict
        json_path = os.path.join(self.images_dir, "Segmix_VOCSegmentation.json")
        with open(json_path, "w") as f:
            json.dump(obj=annotations, fp=f, indent=4)
        print("Done.")
