import torch
import torchvision
import os
import engine


def get_source_dataset(source_name):
    root = os.path.join("/home/dani/datasets", source_name)
    download = not os.path.exists(root)
    ToTensor = torchvision.transforms.ToTensor()
    if source_name == "ImageNet":
        source_dataset = engine.datasets.source.ImageNetDataset(
            root=root, split='train', transform=ToTensor,
        )
    elif source_name == "MNIST":
        source_dataset = torchvision.datasets.MNIST(
            root=root, train=True, download=download,
            transform=ToTensor,
        )
    elif source_name == "CelebA":
        source_dataset = torchvision.datasets.CelebA(
            root=root, split='train', download=download,
            transform=ToTensor,
        )
    elif source_name == "STL10":
        source_dataset = torchvision.datasets.STL10(
            root=root, split='train', download=download,
            transform=ToTensor,
        )
    elif source_name == "VOCSegmentation":
        source_dataset = torchvision.datasets.VOCSegmentation(
            root=root, image_set='train', download=download,
            transform=ToTensor, target_transform=torchvision.transforms.Compose([
                ToTensor,
                torchvision.transforms.Lambda(lambda x: (x * 255).type(torch.int64)),
                torchvision.transforms.Lambda(lambda x: x.squeeze()),
                torchvision.transforms.Lambda(lambda x: torch.where(x == 255, 0, x)),
            ]),
        )
    elif source_name == "WikiArt":
        source_dataset = engine.datasets.source.WikiArtDataset()
    elif source_name == "StyleTransfer3k":
        from PIL import Image
        from engine.datasets.style_transfer.ControlNet import HQ_paths
        all_paths = []
        for val in HQ_paths.values():
            all_paths += val
        class StyleTransfer3k(torch.utils.data.Dataset):
            def __init__(self):
                super(StyleTransfer3k).__init__()
                self.all_paths = sorted(all_paths)
            def __getitem__(self, idx, path=None):
                assert (idx is None) ^ (path is None)
                if path is None:
                    path = self.all_paths[idx]
                image = torchvision.transforms.ToTensor()(Image.open(path))
                return image, path
            def __len__(self):
                return len(self.all_paths)
        source_dataset = StyleTransfer3k()
    elif source_name == "WikiArtSegmentation":
        source_dataset = engine.datasets.source.WikiArtSegmentation()  # using default arguments
    else:
        raise ValueError(f"[ERROR] Source dataset name \"{source_name}\" cannot be handled.")
    assert isinstance(source_dataset, torch.utils.data.Dataset), f"{type(source_dataset)=}"
    return source_dataset
