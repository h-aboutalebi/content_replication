"""
WIKIART API.
"""
from engine.datasets.source.WikiArt.split_options import split_options
from engine.datasets.source.WikiArt.WikiArtDataset import WikiArtDataset
from engine.datasets.source.WikiArt.WikiArtSegmentation import WikiArtSegmentation


__all__ = (
    "split_options",
    "WikiArtDataset",
    "WikiArtSegmentation",
)
