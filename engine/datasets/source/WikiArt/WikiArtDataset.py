import torch
import torchvision
from PIL import Image
import os
import glob

import engine


class WikiArtDataset(torch.utils.data.Dataset):

    def __init__(
            self,
            root="/home/dani/datasets/wikiart",
            split=None,
        ):
        super(WikiArtDataset, self).__init__()
        self.root = root
        assert os.path.exists(self.root)
        if split is None:
            split = "**"
        else:
            assert split in engine.datasets.source.WikiArt.split_options
        self.image_paths = sorted(glob.glob(os.path.join(self.root, split, "*.jpg")))

    def __len__(self):
        return len(self.image_paths)

    def __getitem__(self, idx, path=None):
        assert (idx is None) ^ (path is None)
        if path is None:
            path = self.image_paths[idx]
        image = torchvision.transforms.ToTensor()(Image.open(path))
        return image, path
