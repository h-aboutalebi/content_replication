import numpy
import torch
import torchvision
from PIL import Image
import os
import glob

import engine


class WikiArtSegmentation(torch.utils.data.Dataset):

    def __init__(
        self,
        images_root="/home/dani/datasets/wikiart",
        labels_root="/home/dani/datasets/WikiArtSegmentation",
        split=None
    ):
        super(WikiArtSegmentation, self).__init__()
        self.images_root = images_root
        self.labels_root = labels_root
        if split is None:
            split = "**"
        else:
            assert split in engine.datasets.source.wikiart.split_options
        self.label_paths = sorted(glob.glob(os.path.join(labels_root, split, "*.npy")))

    def __len__(self):
        return len(self.labels_root)

    def label2image(self, path):
        # convert to relative path
        path = '/'.join(path.split('/')[-2:])
        # change extension
        path = '.'.join(path.split('.')[:-1]) + ".jpg"
        # change to absolute path
        path = os.path.join(self.images_root, path)
        return path

    def __getitem__(self, idx):
        label_path = self.label_paths[idx]
        label = torch.from_numpy(numpy.load(label_path)).type(torch.int64)
        image_path = self.label2image(label_path)
        image = torchvision.transforms.ToTensor()(Image.open(image_path))
        return image, label
