import pytest
import torch
import torchvision
import random
import engine


@pytest.mark.parametrize("source_name", [
    pytest.param("MNIST"),
    pytest.param("STL10"),
])
def test_classification_dataset_structure(source_name):
    generator = engine.datasets.mixing.MixGenerator(
        source_name=source_name, config_dir=None,
    )
    dataset = generator.source
    assert isinstance(dataset, torch.utils.data.Dataset), f"{type(dataset)=}"
    example = dataset[random.choice(range(len(dataset)))]
    assert type(example) == tuple
    assert len(example) == 2
    image, label = example
    assert type(image) == torch.Tensor
    assert len(image.shape) == 3
    assert image.dtype == torch.float32
    assert 0 <= torch.min(image) <= torch.max(image) <= 1
    assert type(label) == int


@pytest.mark.parametrize("source_name", [
    pytest.param("VOCSegmentation"),
    # TODO
    # pytest.param("COCOSegmentation"),
])
def test_segmentation_dataset_structure(source_name):
    generator = engine.datasets.mixing.MixGenerator(
        source_name=source_name, config_dir=None,
    )
    dataset = generator.source
    assert isinstance(dataset, torch.utils.data.Dataset), f"{type(dataset)=}"
    example = dataset[random.choice(range(len(dataset)))]
    assert type(example) == tuple
    assert len(example) == 2
    image, label = example
    assert type(image) == torch.Tensor
    assert len(image.shape) == 3
    assert image.dtype == torch.float32
    assert 0 <= torch.min(image) <= torch.max(image) <= 1
    assert type(label) == torch.Tensor
    assert len(label.shape) == 2
    assert label.dtype == torch.int64
