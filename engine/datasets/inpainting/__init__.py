"""
INPAINTING API.
"""
from engine.datasets.inpainting.mask_gen import MaskGenerator


__all__ = (
    "MaskGenerator",
)
