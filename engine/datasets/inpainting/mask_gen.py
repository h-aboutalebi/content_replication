import torch
import torchvision
import os
import math
import random


class MaskGenerator:

    MIN_PERCENTAGE = 0.2
    MAX_PERCENTAGE = 0.5
    MASK_SIZE = 1024  # will be resized to match the image size
    GRID_COUNT = 7
    PATCH_SIZE = 256

    def __init__(self, masks_dir):
        self.masks_dir = masks_dir
        self.GRID_SIZE = int(self.MASK_SIZE // self.GRID_COUNT)

    def gen_masks(self, num_diagonal, num_grid, num_patch):
        self.gen_diagonal(num_diagonal)
        self.gen_grid(num_grid)
        self.gen_patch(num_patch)

    def gen_diagonal(self, num):
        random.seed(0)
        for idx in range(num):
            region = random.choice([0, 1])  # 0 for lower and 1 for upper
            mask = torch.ones((self.MASK_SIZE, self.MASK_SIZE))
            if region == 0:
                mask = torch.tril(mask)
            else:
                mask = torch.triu(mask)
            torchvision.utils.save_image(
                tensor=mask, fp=os.path.join(self.masks_dir, "diagonal", f"sample_{idx}.png"),
            )

    def gen_grid(self, num):
        random.seed(0)
        for idx in range(num):
            num_samples = random.choice(range(
                math.ceil(self.GRID_COUNT**2*self.MIN_PERCENTAGE),
                math.floor(self.GRID_COUNT**2*self.MAX_PERCENTAGE)+1,
            ))
            grid_indices = random.sample(range(self.GRID_COUNT**2), k=num_samples)
            mask = torch.ones((self.MASK_SIZE, self.MASK_SIZE))
            for grid_idx in grid_indices:
                i, j = divmod(grid_idx, self.GRID_COUNT)
                mask[
                    int(i*self.GRID_SIZE):int((i+1)*self.GRID_SIZE),
                    int(j*self.GRID_SIZE):int((j+1)*self.GRID_SIZE)
                    ] = 0
            torchvision.utils.save_image(
                tensor=mask, fp=os.path.join(self.masks_dir, "grid", f"sample_{idx}.png"),
            )

    def gen_patch(self, num):
        random.seed(0)
        for idx in range(num):
            mask = torch.ones((self.MASK_SIZE, self.MASK_SIZE))
            region_size = random.uniform(self.MIN_PERCENTAGE, self.MAX_PERCENTAGE) * self.MASK_SIZE**2
            while 1:
                i, j = random.sample(range(self.MASK_SIZE-self.PATCH_SIZE), k=2)
                new_mask = mask
                new_mask[i:i+self.PATCH_SIZE, j:j+self.PATCH_SIZE] = 0
                new_mask_size = torch.sum(new_mask == 0)
                if new_mask_size > region_size:
                    if new_mask_size < self.MASK_SIZE**2*self.MAX_PERCENTAGE:
                        mask = new_mask
                    break
                else:
                    mask = new_mask
            torchvision.utils.save_image(
                tensor=mask, fp=os.path.join(self.masks_dir, "patch", f"sample_{idx}.png"),
            )
