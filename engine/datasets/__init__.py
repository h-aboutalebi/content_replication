"""
DATASETS API.
"""
from engine.datasets import source
from engine.datasets import cutmix
from engine.datasets import segmix
from engine.datasets import inpainting
from engine.datasets import style_transfer


__all__ = (
    "source",
    "cutmix",
    "segmix",
    "inpainting",
    "style_transfer",
)
