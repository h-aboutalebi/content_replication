import torch
import torchvision
import os
import random
from tqdm import tqdm
import json


class CutmixGen:

    MIN_PATCH_SIZE = 0.4
    MAX_PATCH_SIZE = 0.8

    def __init__(self, source_dataset, config_path, images_dir):
        self.source_dataset = source_dataset
        self.config_path = config_path
        self.images_dir = images_dir
        if not os.path.exists(self.images_dir):
            raise ValueError(f"[ERROR] Images directory path \"{self.images_dir}\" not found.")

    def generate_config(self, num_examples):
        print("Generating config...")
        assert self.source_dataset is not None
        random.seed(0)
        config_dict = {}
        for idx in tqdm(range(num_examples), leave=False):
            s_idx, t_idx = random.sample(range(len(self.source_dataset)), k=2)
            s_cls = self.source_dataset[s_idx][1]
            t_cls = self.source_dataset[t_idx][1]
            s_image_h, s_image_w = self.source_dataset[s_idx][0].shape[-2:]
            t_image_h, t_image_w = self.source_dataset[t_idx][0].shape[-2:]
            patch_size_min = int(min(t_image_h, t_image_w) * self.MIN_PATCH_SIZE)
            patch_size_max = int(min(t_image_h, t_image_w) * self.MAX_PATCH_SIZE)
            patch_size = random.randint(patch_size_min, patch_size_max)
            patch_size = min(s_image_h, s_image_w, patch_size)
            s_loc_i = random.randint(0, s_image_h-patch_size)
            s_loc_j = random.randint(0, s_image_w-patch_size)
            t_loc_i = random.randint(0, t_image_h-patch_size)
            t_loc_j = random.randint(0, t_image_w-patch_size)
            example_dict = {}
            example_dict['s_idx'] = s_idx
            example_dict['t_idx'] = t_idx
            example_dict['s_cls'] = s_cls
            example_dict['t_cls'] = t_cls
            example_dict['patch_size'] = patch_size
            example_dict['s_loc_i'] = s_loc_i
            example_dict['s_loc_j'] = s_loc_j
            example_dict['t_loc_i'] = t_loc_i
            example_dict['t_loc_j'] = t_loc_j
            config_dict[idx] = example_dict
        with open(self.config_path, mode='w') as f:
            json.dump(config_dict, f, indent=4)
        print("Done.")

    def generate_images(self):
        print("Generating images...")
        annotations = {}
        if not os.path.exists(self.config_path):
            raise ValueError(f"[ERROR] Config file not found.")
        with open(self.config_path, mode='r') as f:
            config_dict = json.load(f)
        for idx, example_dict in tqdm(config_dict.items(), leave=False):
            idx = int(idx)
            # source image
            s_image = self.source_dataset[example_dict['s_idx']][0]
            source_1_path = os.path.join(self.images_dir, f"{idx:03d}_source_1.png")
            torchvision.utils.save_image(s_image, source_1_path)
            # target iamge
            t_image = self.source_dataset[example_dict['t_idx']][0]
            source_2_path = os.path.join(self.images_dir, f"{idx:03d}_source_2.png")
            torchvision.utils.save_image(t_image, source_2_path)
            # generated image
            generated = t_image
            generated[:, example_dict['t_loc_i']:example_dict['t_loc_i']+example_dict['patch_size'], example_dict['t_loc_j']:example_dict['t_loc_j']+example_dict['patch_size']] = s_image[:, example_dict['s_loc_i']:example_dict['s_loc_i']+example_dict['patch_size'], example_dict['s_loc_j']:example_dict['s_loc_j']+example_dict['patch_size']]
            generated_path = os.path.join(self.images_dir, f"{idx:03d}_generated.png")
            torchvision.utils.save_image(generated, generated_path)
            # annotation
            example_dict = {}
            example_dict['source_1'] = os.path.join("/home/dani/repos/content_replication/DeepfakeArt/similar/cutmix", f"{idx:03d}_source_1.png")
            example_dict['source_2'] = os.path.join("/home/dani/repos/content_replication/DeepfakeArt/similar/cutmix", f"{idx:03d}_source_2.png")
            example_dict['generated'] = os.path.join("/home/dani/repos/content_replication/DeepfakeArt/similar/cutmix", f"{idx:03d}_generated.png")
            annotations[idx] = example_dict
        json_path = "/home/dani/repos/content_replication/DeepfakeArt/cutmix.json"
        with open(json_path, "w") as f:
            json.dump(annotations, f, indent=4)
        print("Done.")
