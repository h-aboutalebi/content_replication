"""
CUTMIX API.
"""
from engine.datasets.cutmix.CutmixGen import CutmixGen


__all__ = (
    "CutmixGen",
)
