"""
PIPELINES API.
"""
from engine.pipelines.evaluate import evaluate_model


__all__ = (
    "evaluate_model",
)
