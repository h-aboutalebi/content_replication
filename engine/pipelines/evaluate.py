import torch
from tqdm import tqdm
from typing import Callable


def evaluate_model(model, dataloader, device, logger, metric, similarity_list, get_embedding: Callable=lambda x: x):
    model.eval()
    model.to(device)
    score = 0
    tp, tn, fp, fn = 0, 0, 0, 0
    all_metrics = {}
    for m in metric:
        all_metrics[m.metric_name()] = 0
    for (images_0, images_1), infos in tqdm(dataloader, leave=False):
        images_0 = images_0.to(device)
        images_1 = images_1.to(device)
        labels = infos["label"].to(device)
        embeddings_0 = get_embedding(model(images_0)) #['normalized_embedding']
        embeddings_1 = get_embedding(model(images_1)) #['normalized_embedding']
        similarity = torch.sum(embeddings_0 * embeddings_1, dim=1, keepdim=False)
        for m in metric:
            all_metrics[m.metric_name()] += m(similarity=similarity, ground_truth=labels).item()
        similarity_list += similarity.tolist()
    return all_metrics
