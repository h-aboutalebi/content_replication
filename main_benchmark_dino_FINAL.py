import torch
import torchvision
import os
from datetime import datetime
import matplotlib.pyplot as plt
import gc

from models.dinov2.hubconf import *
from models.dino.hubconf import *
from models.MultiGrain.multigrain.lib import get_multigrain
from models.SSCD.sscd.models.model import Model as SSCD
from DeepfakeArtDataset import DeepfakeArtDataset
import engine
import utils
from metrics import TPCount, TNCount, FPCount, FNCount, GeneralMetrics
from transformers import AutoImageProcessor, SwinForImageClassification

gpu2sampler = {
    7: { "similar": { "inpainting": 'all' }},
    6: { "similar": { "style_transfer": 'all' }},
    5: { "similar": { "adversarial": 'all' }},
    4: { "similar": { "cutmix": 'all' }},
    3: { "dissimilar": { "original": 'all' }},
    2: { "dissimilar": { "inpainting": 'all' }},
    1: { "dissimilar": { "style_transfer": 'all' }},
    0: { "dissimilar": { "adversarial": 'all' }},
}

datasets_info = [
    ("dissimilar", "adversarial"),
    ("dissimilar", "style_transfer"),
    ("dissimilar", "inpainting"),
    ("dissimilar", "original"),
    ("similar", "cutmix"),
    ("similar", "adversarial"),
    ("similar", "style_transfer"),
    ("similar", "inpainting")
]

model_name = {
    0: "dino_vits16",
    1: "dino_vits8",
    2: "dino_vitb16",
    3: "dino_vitb8",
    4: "dino_xcit_small_12_p16",
    5: "dino_xcit_small_12_p8",
    6: "dino_xcit_medium_24_p16",
    7: "dino_xcit_medium_24_p8",
    8: "dino_resnet50"
}

#dino modelname; txtfile import; plot path missing; gpu2sampler

def main(args):
    time = datetime.now().strftime("%d%m%Y_%H%M%S")
    logger = utils.get_logger(filepath=os.path.join("logs", f"{time}.txt"))
    device = torch.device(f"cuda:{args.dataset_id}")

    get_embedding = lambda x: torch.nn.functional.normalize(x, p=2, dim=1)
    {model_name[args.model_id]}
    model = eval(f"{model_name[args.model_id]}()")
    dino_name = model_name[args.model_id]

    dataset = DeepfakeArtDataset(
        root="/home/dani/repos/content_replication/DeepfakeArt",
        sampler=gpu2sampler[args.dataset_id], seed=0,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Resize(size=(256,)*2),
        ]),
    )

    dataloader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=4)

    metric = [
        TPCount(threshold=0.50),
        TNCount(threshold=0.50),
        FPCount(threshold=0.50),
        FNCount(threshold=0.50),
    ]
    similarity_list = []
    all_metrics = engine.pipelines.evaluate_model(
        model=model, dataloader=dataloader, device=device, logger=logger, metric=metric, similarity_list=similarity_list, get_embedding=get_embedding
    )

    generalMetrics = GeneralMetrics(all_metrics)
    print(f"dataset_info: {datasets_info[args.dataset_id]}")
    print(f"accuracy = {generalMetrics.get_accuracy()}")
    print(f"precision = {generalMetrics.get_precision()}")
    print(f"recall = {generalMetrics.get_recall()}")
    print(f"F1 = {generalMetrics.get_f1()}")
    print(generalMetrics.__str__())

    write_dir = f'plots/Dino-Transformer{dino_name}'
    if not os.path.exists(f"{write_dir}"):
        os.mkdir(f"{write_dir}")
    with open(f"{write_dir}/{args.dataset_id}_{args.metric_log_path}.log", 'w') as f:
        f.write(f"dataset_info: {datasets_info[args.dataset_id]}\n")
        f.write(f"accuracy = {generalMetrics.get_accuracy()}\n")
        f.write(f"precision = {generalMetrics.get_precision()}\n")
        f.write(f"recall = {generalMetrics.get_recall()}\n")
        f.write(f"F1 = {generalMetrics.get_f1()}\n")
        f.write(generalMetrics.__str__())
    
    path_noext = f"{write_dir}/{args.dataset_id}_score_distribution"

    torch.save(torch.Tensor(similarity_list), path_noext+".pt")
    plt.figure()
    plt.hist(similarity_list, bins=50)
    plt.title(f"Dino-Transformer {args.dataset_id}")
    plt.savefig(path_noext+".png")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gpu_id', type=int, default=4,
    )
    parser.add_argument(
        '--model_id', type=int, default=2,
    )
    parser.add_argument(
        '--dataset_id', type=int, default=0,
    )

    parser.add_argument(
        '--metric_log_path', type=str, default='metric',
    )
    
    args = parser.parse_args()
    main(args)

