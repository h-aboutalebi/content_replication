import os
import glob
import random
from tqdm import tqdm
import json


def generate():
    random.seed(1)  # Note that I was using random.seed(0) for Cutmix
    dataset_root = "/home/dani/repos/content_replication/DeepfakeArt"
    paths = {
        "original": sorted(glob.glob(os.path.join(dataset_root, "similar", "style_transfer/**/*_original.png"))),
        "inpainting": sorted(glob.glob(os.path.join(dataset_root, "similar", "inpainting/**/inpainting.png"))),
        "style_transfer": sorted(glob.glob(os.path.join(dataset_root, "similar", "style_transfer/**/*_generated.png"))),
        "adversarial": sorted(glob.glob(os.path.join(dataset_root, "similar", "adversarial/**/adv_*.png"))),
    }
    annotations = {}

    category_dict = {}
    print("Generating (original, original) pairs...")
    for idx in tqdm(range(5000), leave=False):
        image_0_path, image_1_path = random.sample(paths['original'], k=2)
        category_dict[idx] = {
            'image_0': os.path.relpath(image_0_path, start=dataset_root),
            'image_1': os.path.relpath(image_1_path, start=dataset_root),
        }
    annotations['original'] = category_dict

    for category in ["inpainting", "style_transfer", "adversarial"]:
        category_dict = {}
        print(f"Generating (original, {category}) pairs...")
        for idx in tqdm(range(5000), leave=False):
            image_0_path = random.choice(paths['original'])
            image_1_path = random.choice(paths[category])
            category_dict[idx] = {
                'image_0': os.path.relpath(image_0_path, start=dataset_root),
                'image_1': os.path.relpath(image_1_path, start=dataset_root),
            }
        annotations[category] = category_dict

    save_path = "/home/dani/repos/content_replication/DeepfakeArt/dissimilar/dissimilar.json"
    with open(save_path, mode='w') as f:
        json.dump(obj=annotations, fp=f, indent=4)


if __name__ == "__main__":
    generate()
