import torchvision
import torchattacks
from robustbench import load_model
import torch
import os
import glob
from PIL import Image
import os
from tools import restore_image, default_loader
from tqdm import tqdm


os.environ["CUDA_VISIBLE_DEVICES"] = "3"
model = load_model(model_name="Standard_R50", dataset='imagenet', threat_model='Linf', model_dir=".")
model = model.to("cuda")


class MyCustomDataset(torch.utils.data.Dataset):

    def __init__(self, images_root):
        super(MyCustomDataset, self).__init__()
        self.all_paths = sorted(glob.glob(os.path.join(images_root, "*.jpg")), key=lambda x: int(os.path.basename(x).split('.')[0]))
        self.loader = default_loader
        self.transform = torchvision.transforms.Compose([
            torchvision.transforms.Resize(size=(224, 224)),
            torchvision.transforms.ToTensor(),
        ])

    def __len__(self):
        return len(self.all_paths)

    def __getitem__(self, idx):
        path = self.all_paths[idx]
        return self.transform(self.loader(path)), path

        
source_images_root = "/home/hossein/github/content_replication/result_adv/val/n01440764"
dataset = MyCustomDataset(images_root=source_images_root)
batch_size = 128
torch.manual_seed(0)
dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True)
save_outputs_root_dir = "/home/dani/repos/content_replication/DeepfakeArt/similar/adversarial"
idx = 0
pbar = tqdm(total=len(dataset), leave=False)
for image_batch, path_batch in dataloader:
    image_batch = image_batch.to("cuda")
    attack_fgsm = torchattacks.FGSM(model, eps=8/255)
    attack_APGD = torchattacks.APGD(model, norm='Linf', eps=8/255, steps=10, n_restarts=1, seed=0, loss='ce', eot_iter=1, rho=.75, verbose=False)
    attack_PGD = torchattacks.PGD(model, eps=8/255, alpha=1/255, steps=10, random_start=True)
    adv_images_fgsm = attack_fgsm(image_batch, torch.zeros(len(image_batch), dtype=torch.int64).to("cuda"))
    adv_images_APGD = attack_APGD(image_batch, torch.zeros(len(image_batch), dtype=torch.int64).to("cuda"))
    adv_images_PGD = attack_PGD(image_batch, torch.zeros(len(image_batch), dtype=torch.int64).to("cuda"))
    for idx_in_batch in range(len(image_batch)):
        subdir_for_idx = os.path.join(save_outputs_root_dir, str(idx))
        if not os.path.exists(subdir_for_idx):
            os.makedirs(subdir_for_idx)
        # copy original image from source location to target location
        image = Image.open(path_batch[idx_in_batch])
        image.save(os.path.join(subdir_for_idx, "original.png"))
        # resize and save adversarial images
        adv_images_reconstructed = restore_image(adv_images_fgsm[idx_in_batch].to("cpu"),image.size[0],image.size[1])
        adv_images_reconstructed.save(os.path.join(subdir_for_idx, "adv_FGSM.png"))
        adv_images_reconstructed = restore_image(adv_images_APGD[idx_in_batch].to("cpu"),image.size[0],image.size[1])
        adv_images_reconstructed.save(os.path.join(subdir_for_idx, "adv_APGD.png"))
        adv_images_reconstructed = restore_image(adv_images_PGD[idx_in_batch].to("cpu"),image.size[0],image.size[1])
        adv_images_reconstructed.save(os.path.join(subdir_for_idx, "adv_PGD.png"))
        # increment global counter
        idx += 1
        pbar.update(1)
