import os
import random
from PIL import Image
import numpy as np
import torch

def restore_image(image, width, height):
    img_array = image.numpy()
    img_array = np.transpose(img_array, (1, 2, 0))
    img_array = (img_array * 255).astype(np.uint8)
    img = Image.fromarray(img_array)
    img_resized = img.resize((width, height), Image.ANTIALIAS)
    return img_resized


def pil_loader(path):
    # open path as file to avoid ResourceWarning (https://github.com/python-pillow/Pillow/issues/835)
    with open(path, 'rb') as f:
        img = Image.open(f)
        return img.convert('RGB')


def accimage_loader(path):
    import accimage
    try:
        return accimage.Image(path)
    except IOError:
        # Potentially a decoding problem, fall back to PIL.Image
        return pil_loader(path)


def default_loader(path):
    from torchvision import get_image_backend
    if get_image_backend() == 'accimage':
        return accimage_loader(path)
    else:
        return pil_loader(path)

