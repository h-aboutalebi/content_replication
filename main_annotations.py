import os
import glob
import json
from tqdm import tqdm

import sys
sys.path.append("..")

import engine
from engine.datasets.style_transfer.ControlNet import HQ_512
from engine.datasets.style_transfer.ControlNet import HQ_1024
from engine.datasets.style_transfer.ControlNet import HQ_more_1024
from scripts_style_transfer.main import idx2prompt


def gen_ann_inpainting():
    print("Generating annotations for inpainting...")
    inpainting_results_root = "/home/dani/repos/content_replication/DeepfakeArt/similar/inpainting"
    annotations = {}
    for idx, dir_name in enumerate(sorted(os.listdir(inpainting_results_root), key=lambda x: int(x))):
        annotations[idx] = {
            "original": f"similar/inpainting/{dir_name}/original.png",
            "mask": f"similar/inpainting/{dir_name}/mask.png",
            "generated": f"similar/inpainting/{dir_name}/inpainting.png",
        }
    return annotations


def gen_ann_style_transfer():
    print("Generating annotations for style transfer...")
    split_options = engine.datasets.source.WikiArt.split_options
    idx_configs = dict(
        (split, sorted(list(HQ_512[split] | HQ_1024[split] | HQ_more_1024[split])))
        for split in split_options
    )
    annotations = {}
    global_idx = 0
    for split in split_options:
        for idx in idx_configs[split]:
            annotations[global_idx] = {
                "original": f"similar/style_transfer/{split}/{idx:03d}_original.png",
                "edges": f"similar/style_transfer/{split}/{idx:03d}_edges.png",
                "generated": f"similar/style_transfer/{split}/{idx:03d}_generated.png",
                "prompt": idx2prompt(idx=idx, dataset_size=len(glob.glob(f"/home/dani/datasets/wikiart/{split}/*.jpg"))),
                "resolution": 512 if idx in HQ_512[split] else 1024,
            }
            global_idx += 1
    return annotations


def gen_ann_adversarial():
    print("Generating annotations for adversarial...")
    root_dir = "/home/dani/repos/content_replication/DeepfakeArt/similar/adversarial"
    annotations = {}
    for idx, dir_name in enumerate(sorted(os.listdir(root_dir), key=lambda x: int(x))):
        annotations[3*idx] = {
            'original': os.path.join("similar", "adversarial", dir_name, "original.png"),
            'generated': os.path.join("similar", "adversarial", dir_name, "adv_APGD.png"),
        }
        annotations[3*idx+1] = {
            'original': os.path.join("similar", "adversarial", dir_name, "original.png"),
            'generated': os.path.join("similar", "adversarial", dir_name, "adv_FGSM.png"),
        }
        annotations[3*idx+2] = {
            'original': os.path.join("similar", "adversarial", dir_name, "original.png"),
            'generated': os.path.join("similar", "adversarial", dir_name, "adv_PGD.png"),
        }
    return annotations


def gen_ann_cutmix():
    print("Generating annotations for cutmix...")
    cutmix_results_dir = "/home/dani/repos/content_replication/DeepfakeArt/similar/cutmix"
    annotations = {}
    all_indices = set(list(map(lambda x: int(x.split('_')[0]), os.listdir(cutmix_results_dir))))
    for idx in all_indices:
        annotations[2*idx] = {
            "original": f"similar/cutmix/{idx:03d}_source_1.png",
            "generated": f"similar/cutmix/{idx:03d}_generated.png",
        }
        annotations[2*idx+1] = {
            "original": f"similar/cutmix/{idx:03d}_source_2.png",
            "generated": f"similar/cutmix/{idx:03d}_generated.png",
        }
    return annotations


if __name__ == "__main__":
    annotations = {
        'inpainting': gen_ann_inpainting(),
        'style_transfer': gen_ann_style_transfer(),
        'adversarial': gen_ann_adversarial(),
        'cutmix': gen_ann_cutmix(),
    }
    save_path = "/home/dani/repos/content_replication/DeepfakeArt/similar/similar.json"
    with open(save_path, mode='w') as f:
        json.dump(obj=annotations, fp=f, indent=4)
