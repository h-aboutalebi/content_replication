import engine


source_dataset = engine.datasets.source.get_source_dataset("StyleTransfer3k")
generator = engine.datasets.cutmix.CutmixGen(
    source_dataset=source_dataset,
    config_path="/home/dani/repos/content_replication/engine/datasets/cutmix/config_CutmixGen_WikiArt.json",
    images_dir="/home/dani/repos/content_replication/DeepfakeArt/similar/cutmix",
)
generator.generate_config(num_examples=1000)
generator.generate_images()
