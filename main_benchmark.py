import torch
import torchvision
import os
from datetime import datetime
import matplotlib.pyplot as plt

from models.MultiGrain.multigrain.lib import get_multigrain
from models.SSCD.sscd.models.model import Model as SSCD
from DeepfakeArtDataset import DeepfakeArtDataset
import engine
import utils
from metrics import TPCount, TNCount, FPCount, FNCount, GeneralMetrics
from transformers import AutoImageProcessor, SwinForImageClassification

gpu2sampler = {
    7: { "similar": { "inpainting": 'all' }},
    6: { "similar": { "style_transfer": 'all' }},
    5: { "similar": { "adversarial": 'all' }},
    4: { "similar": { "cutmix": 'all' }},
    3: { "dissimilar": { "original": 'all' }},
    2: { "dissimilar": { "inpainting": 'all' }},
    1: { "dissimilar": { "style_transfer": 'all' }},
    0: { "dissimilar": { "adversarial": 'all' }},
}

datasets_info = [
    ("dissimilar", "adversarial"),
    ("dissimilar", "style_transfer"),
    ("dissimilar", "inpainting"),
    ("dissimilar", "original"),
    ("similar", "cutmix"),
    ("similar", "adversarial"),
    ("similar", "style_transfer"),
    ("similar", "inpainting")
]




def main(args):
    time = datetime.now().strftime("%d%m%Y_%H%M%S")
    logger = utils.get_logger(filepath=os.path.join("logs", f"{time}.txt"))
    device = torch.device(f"cuda:{args.gpu_id}")

    # model = get_multigrain('resnet50')
    # # checkpoint = torch.load("/home/dani/repos/content_replication/models/MultiGrain/checkpoints/joint_3BAA_0.5.pth")
    # checkpoint = torch.load("/home/richardf2023/content_replication/models/MultiGrain/checkpoints/joint_3BAA_0.5.pth")
    # model.load_state_dict(checkpoint['model_state'])
    # get_embedding = lambda x: x['normalized_embedding']

    # model = SSCD("CV_RESNET50", 512, 3.0)
    # weights = torch.load("/home/dani/repos/content_replication/models/SSCD/checkpoints/sscd_disc_mixup.classy.pt")
    # model.load_state_dict(weights)
    # get_embedding = lambda x: x['normalized_embedding']
    # get_embedding = lambda x: x['normalized_embedding']
    
    get_embedding = lambda x: torch.nn.functional.normalize(x.logits)
    model = SwinForImageClassification.from_pretrained("microsoft/swin-tiny-patch4-window7-224")

    dataset = DeepfakeArtDataset(
        root="/home/dani/repos/content_replication/DeepfakeArt",
        sampler=gpu2sampler[args.dataset_id], seed=0,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Resize(size=(512,)*2),
        ]),
    )

    dataloader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=4)

    metric = [
        TPCount(threshold=0.50),
        TNCount(threshold=0.50),
        FPCount(threshold=0.50),
        FNCount(threshold=0.50),
    ]
    similarity_list = []


    all_metrics = engine.pipelines.evaluate_model(
        model=model, dataloader=dataloader, device=device, logger=logger, metric=metric, similarity_list=similarity_list, get_embedding=get_embedding
    )

    generalMetrics = GeneralMetrics(all_metrics)
    print(f"dataset_info: {datasets_info[args.dataset_id]}")
    print(f"accuracy = {generalMetrics.get_accuracy()}")
    print(f"precision = {generalMetrics.get_precision()}")
    print(f"recall = {generalMetrics.get_recall()}")
    print(f"F1 = {generalMetrics.get_f1()}")
    print(generalMetrics.__str__())


    write_dir = 'plots/Swin-Transformer'
    with open(f"{write_dir}/{args.dataset_id}_{args.metric_log_path}.log", 'w') as f:
        f.write(f"dataset_info: {datasets_info[args.dataset_id]}\n")
        f.write(f"accuracy = {generalMetrics.get_accuracy()}\n")
        f.write(f"precision = {generalMetrics.get_precision()}\n")
        f.write(f"recall = {generalMetrics.get_recall()}\n")
        f.write(f"F1 = {generalMetrics.get_f1()}\n")
        f.write(generalMetrics.__str__())


    # print(f"acc={correct_count/len(dataset)}")
    
    # path_noext = f"plots/SSCD/score_distribution_{args.gpu_id}"
    # path_noext = f"plots/MultiGrain/score_distribution_{args.dataset_id}"
    path_noext = f"{write_dir}/{args.dataset_id}_score_distribution"
    # path_noext = f"plots/Swin-Transformer/score_distribution_1"
    
    torch.save(torch.Tensor(similarity_list), path_noext+".pt")
    plt.figure()
    plt.hist(similarity_list, bins=50)
    # plt.title(f"SSCD {args.gpu_id}")
    plt.title(f"Swin-Transformer {args.dataset_id}")
    # plt.title(f"MultiGrain {args.gpu_id}")
    plt.savefig(path_noext+".png")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gpu_id', type=int, default=4,
    )
    parser.add_argument(
        '--dataset_id', type=int, default=0,
    )

    parser.add_argument(
        '--metric_log_path', type=str, default='metric',
    )
    
    args = parser.parse_args()
    main(args)
