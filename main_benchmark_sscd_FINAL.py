import torch
import torchvision
import os
from datetime import datetime
import matplotlib.pyplot as plt

from models.MultiGrain.multigrain.lib import get_multigrain
from models.SSCD.sscd.models.model import Model as SSCD
from DeepfakeArtDataset import DeepfakeArtDataset
import engine
import utils
import metrics
from metrics import TPCount, TNCount, FPCount, FNCount, GeneralMetrics
from transformers import AutoImageProcessor, SwinForImageClassification


gpu2sampler = {
    7: { "similar": { "inpainting": 'all' }},
    6: { "similar": { "style_transfer": 'all' }},
    5: { "similar": { "adversarial": 'all' }},
    4: { "similar": { "cutmix": 'all' }},
    3: { "dissimilar": { "original": 'all' }},
    2: { "dissimilar": { "inpainting": 'all' }},
    1: { "dissimilar": { "style_transfer": 'all' }},
    0: { "dissimilar": { "adversarial": 'all' }},
}

gpu2sampler_name = {
    7: "similar_inpainting",
    6: "similar_style_transfer",
    5: "similar_adversarial",
    4: "similar_cutmix",
    3: "dissimilar_original",
    2: "dissimilar_inpainting",
    1: "dissimilar_style_transfer",
    0: "dissimilar_adversarial",
}

model_name = {
    0: "sscd_disc_advanced",
    1: "sscd_disc_blur",
    2: "sscd_disc_large",
    3: "sscd_disc_mixup",
    4: "sscd_imagenet_advanced",
    5: "sscd_imagenet_blur",
    6: "sscd_imagenet_mixup"
}

datasets_info = [
    ("dissimilar", "adversarial"),
    ("dissimilar", "style_transfer"),
    ("dissimilar", "inpainting"),
    ("dissimilar", "original"),
    ("similar", "cutmix"),
    ("similar", "adversarial"),
    ("similar", "style_transfer"),
    ("similar", "inpainting")
]

def main(args):
    time = datetime.now().strftime("%d%m%Y_%H%M%S")
    logger = utils.get_logger(filepath=os.path.join("logs", f"{time}.txt"))
    device = torch.device(f"cuda:{args.gpu_id}")

    model = SSCD("CV_RESNET50", 512, 3.0) #CV_RESNEXT101
    weights = torch.load(f"/home/chrishe/content_replication/models/SSCD/checkpoints/{model_name[args.model_id]}.classy.pt")
    model.load_state_dict(weights)
    get_embedding = lambda x: x['normalized_embedding']


    dataset = DeepfakeArtDataset(
        root="/home/dani/repos/content_replication/DeepfakeArt",
        sampler=gpu2sampler[args.gpu_id], seed=0,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Resize(size=(512,)*2),
        ]),
    )

    dataloader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=2)

    metric = [
        TPCount(threshold=0.50),
        TNCount(threshold=0.50),
        FPCount(threshold=0.50),
        FNCount(threshold=0.50),
    ]
    similarity_list = []

    all_metrics = engine.pipelines.evaluate_model(
        model=model, dataloader=dataloader, device=device, logger=logger, metric=metric, similarity_list=similarity_list, get_embedding=get_embedding
    )

    generalMetrics = GeneralMetrics(all_metrics)
    print(f"dataset_info: {datasets_info[args.dataset_id]}")
    print(f"accuracy = {generalMetrics.get_accuracy()}")
    print(f"precision = {generalMetrics.get_precision()}")
    print(f"recall = {generalMetrics.get_recall()}")
    print(f"F1 = {generalMetrics.get_f1()}")
    print(generalMetrics.__str__())

    write_dir = f'plots/SSCD_{model_name[args.model_id]}'
    if not os.path.exists(f"{write_dir}"):
        os.mkdir(f"{write_dir}")
    with open(f"{write_dir}/{args.dataset_id}_{args.metric_log_path}.log", 'w') as f:
        f.write(f"dataset_info: {datasets_info[args.dataset_id]}\n")
        f.write(f"accuracy = {generalMetrics.get_accuracy()}\n")
        f.write(f"precision = {generalMetrics.get_precision()}\n")
        f.write(f"recall = {generalMetrics.get_recall()}\n")
        f.write(f"F1 = {generalMetrics.get_f1()}\n")
        f.write(generalMetrics.__str__())

    path_noext = f"{write_dir}/{args.dataset_id}_score_distribution"
    
    torch.save(torch.Tensor(similarity_list), path_noext+".pt")
    plt.figure()
    plt.hist(similarity_list, bins=50)
    plt.title(f"SSCD-Transformer {args.dataset_id}")
    plt.savefig(path_noext+".png")


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gpu_id', type=int, default=2,
    )
    parser.add_argument(
        '--model_id', type=int, default=2,
    )
    print("here==============================")
    print(parser.parse_args().gpu_id)
    parser.add_argument(
        '--dataset_id', type=int, default=parser.parse_args().gpu_id,
    )

    parser.add_argument(
        '--metric_log_path', type=str, default='metric',
    )
    
    args = parser.parse_args()
    main(args)
