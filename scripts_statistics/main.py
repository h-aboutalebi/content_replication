import os
import glob
import json
import matplotlib.pyplot as plt


def plot_similar_distribution():
    plt.figure()
    inpainting_count = len(os.listdir("/home/dani/repos/content_replication/DeepfakeArt/similar/inpainting"))
    style_transfer_count = len(glob.glob(os.path.join("/home/dani/repos/content_replication/DeepfakeArt/similar/style_transfer", "**/*_generated.png")))
    adversarial_count = 3 * len(os.listdir("/home/dani/repos/content_replication/DeepfakeArt/similar/adversarial"))
    cutmix_count = 2 * len(glob.glob(os.path.join("/home/dani/repos/content_replication/DeepfakeArt/similar/cutmix", "*_generated.png")))
    plt.pie(
        [inpainting_count, style_transfer_count, adversarial_count, cutmix_count],
        labels=[f"inpainting {inpainting_count}", f"style transfer {style_transfer_count}", f"adversarial {adversarial_count}", f"cutmix {cutmix_count}"],
        startangle=90,
    )
    plt.savefig("similar_distribution.png")
    total = inpainting_count + style_transfer_count + adversarial_count + cutmix_count
    with open("/home/dani/repos/content_replication/DeepfakeArt/similar/similar.json", mode='r') as f:
        annotations = json.load(f)
    assert inpainting_count == max(list(map(lambda x: int(x), annotations['inpainting'].keys()))) + 1
    # assert style_transfer_count == sum([max(list(map(lambda x: int(x), annotations['style_transfer'][split].keys()))) + 1
    #     for split in annotations['style_transfer'].keys()])
    assert adversarial_count == max(list(map(lambda x: int(x), annotations['adversarial'].keys()))) + 1
    assert cutmix_count == max(list(map(lambda x: int(x), annotations['cutmix'].keys()))) + 1
    return total


def plot_dissimilar_distribution():
    plt.figure()
    original_count = 5000
    inpainting_count = 5000
    style_transfer_count = 5000
    adversarial_count = 5000
    plt.pie(
        [original_count, inpainting_count, style_transfer_count, adversarial_count],
        labels=[f"(original, original) {original_count}", f"(original, inpainting) {inpainting_count}", f"(original, style transfer) {style_transfer_count}", f"(original, adversarial) {adversarial_count}"],
        startangle=90,
    )
    plt.savefig("dissimilar_distribution.png")
    total = original_count + inpainting_count + style_transfer_count + adversarial_count
    with open("/home/dani/repos/content_replication/DeepfakeArt/dissimilar/dissimilar.json", mode='r') as f:
        annotations = json.load(f)
    assert original_count == max(list(map(lambda x: int(x), annotations['inpainting'].keys()))) + 1
    assert inpainting_count == max(list(map(lambda x: int(x), annotations['inpainting'].keys()))) + 1
    # assert style_transfer_count == sum([max(list(map(lambda x: int(x), annotations['style_transfer'][split].keys()))) + 1
    #     for split in annotations['style_transfer'].keys()])
    assert adversarial_count == max(list(map(lambda x: int(x), annotations['adversarial'].keys()))) + 1
    return total


def plot_total(similar_total, dissimilar_total):
    plt.figure()
    plt.pie(
        [similar_total, dissimilar_total],
        labels=[f"similar total {similar_total}", f"dissimilar total {dissimilar_total}"],
        startangle=90,
    )
    plt.savefig("total_distribution.png")


if __name__ == "__main__":
    similar_total = plot_similar_distribution()
    dissimilar_total = plot_dissimilar_distribution()
    plot_total(similar_total=similar_total, dissimilar_total=dissimilar_total)
