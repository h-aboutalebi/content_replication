import torch
import torchvision
from PIL import Image
import os
import json
import random


class DeepfakeArtDataset(torch.utils.data.Dataset):

    def __init__(
        self,
        root="/home/dani/repos/content_replication/DeepfakeArt",
        sampler=None,
        seed=None,
        transform=None,
    ):
        super(DeepfakeArtDataset, self).__init__()
        self.root = root
        # read in annotations
        with open(os.path.join(root, "similar", "similar.json"), mode='r') as f:
            self.sim_dict = json.load(f)
        with open(os.path.join(root, "dissimilar", "dissimilar.json"), mode='r') as f:
            self.dis_dict = json.load(f)
        self.annotations = {"similar": self.sim_dict, "dissimilar": self.dis_dict}
        # construct subsets
        self.subset_sizes = []
        self.subsets = []
        if sampler is None:
            sampler = {}
        if seed is not None:
            assert type(seed) == int
            random.seed(seed)
        assert type(sampler) == dict
        print(f"{self.annotations.keys()=}")
        for idx1, key1 in enumerate(self.annotations.keys()):
            print(f"{self.annotations[key1].keys()=}")
            for idx2, key2 in enumerate(self.annotations[key1].keys()):
                if key1 in sampler.keys() and key2 in sampler[key1].keys():
                    subset = list(self.annotations[key1][key2].items())
                    if type(sampler[key1][key2]) == int:
                        self.subsets.append(random.choices(population=subset, k=sampler[key1][key2]))
                        self.subset_sizes.append(sampler[key1][key2])
                    elif sampler[key1][key2] == 'all':
                        self.subsets.append(subset)
                        self.subset_sizes.append(len(subset))
                    else:
                        raise ValueError()
                else:
                    self.subsets.append([])
                    self.subset_sizes.append(0)
        assert len(self.subsets) == len(self.subset_sizes) == 8
        print(f"{self.subset_sizes=}")
        self.total_size = sum(self.subset_sizes)
        # define data transform
        self.transform = transform if type(transform) == torchvision.transforms.Compose else lambda x: x

    def __getitem__(self, idx):
        for subset_idx, (size, subset) in enumerate(zip(self.subset_sizes, self.subsets)):
            if idx >= size:
                idx -= size
            else:
                example = subset[idx][1]
                label = subset_idx < 4
                break
        relpath0 = example["original"] if example.get("original", None) else example["image_0"]
        relpath1 = example["generated"] if example.get("generated", None) else example["image_1"]
        image1 = self.transform(Image.open(os.path.join(self.root, relpath1)))
        image0 = self.transform(Image.open(os.path.join(self.root, relpath0)))
        info = {
            "label": label,
        }
        return (image0, image1), info

    def __len__(self):
        return self.total_size
