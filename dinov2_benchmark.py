import torch
import torchvision
import os
from datetime import datetime
import matplotlib.pyplot as plt
from transformers import ViTImageProcessor, ViTModel
from PIL import Image
import requests
import json

from models.dinov2.hubconf import *
from models.MultiGrain.multigrain.lib import get_multigrain
from models.SSCD.sscd.models.model import Model as SSCD
from DeepfakeArtDataset import DeepfakeArtDataset
import engine
import utils
import metrics


gpu2sampler = {
    7: { "similar": { "inpainting": 'all' }},
    6: { "similar": { "style_transfer": 'all' }},
    5: { "similar": { "adversarial": 'all' }},
    4: { "similar": { "cutmix": 'all' }},
    3: { "dissimilar": { "original": 'all' }},
    2: { "dissimilar": { "inpainting": 'all' }},
    1: { "dissimilar": { "style_transfer": 'all' }},
    0: { "dissimilar": { "adversarial": 'all' }},
}

sscd_model_name = {
    0: "sscd_disc_advanced",
    1: "sscd_disc_blur",
    2: "sscd_disc_large",
    3: "sscd_disc_mixup",
    4: "sscd_imagenet_advanced",
    5: "sscd_imagenet_blur",
    6: "sscd_imagenet_mixup"
}

model_collect = {
    0: "SSCD",
    1: "dino"
}

dino_model_name = {
    0: "dino_vits16",
    1: "dino_vits8",
    2: "dino_vitb16",
    3: "dino_vitb8",
    4: "dino_xcit_small_12_p16",
    5: "dino_xcit_small_12_p8",
    6: "dino_xcit_medium_24_p16",
    7: "dino_xcit_medium_24_p8",
    8: "dino_resnet50"
}


def main(args):
    if not os.path.exists(f"plots"):
        os.mkdir(f"plots")
    if not os.path.exists(f"plots/{dino_model_name[args.model_id]}_{args.model_id}"):
        os.mkdir(f"plots/{dino_model_name[args.model_id]}_{args.model_id}")
    time = datetime.now().strftime("%d%m%Y_%H%M%S")
    logger = utils.get_logger(filepath=os.path.join("logs", f"{time}.txt"))
    device = torch.device(f"cuda:{args.gpu_id}")

    # model = get_multigrain('resnet50')
    # checkpoint = torch.load("/home/dani/repos/content_replication/models/MultiGrain/checkpoints/joint_3BAA_0.5.pth")
    # model.load_state_dict(checkpoint['model_state'])

    # model = SSCD("CV_RESNET50", 512, 3.0)
    # weights = torch.load("/home/dani/repos/content_replication/models/SSCD/checkpoints/sscd_disc_mixup.classy.pt")
    # model.load_state_dict(weights)
    # model = lambda x: model(x)['normalized_embedding']
    # model = ViTModel.from_pretrained('facebook/dino-vits8')
    model = dinov2_vits14()
    print(model)

    dataset = DeepfakeArtDataset(
        root="/home/dani/repos/content_replication/DeepfakeArt",
        sampler=gpu2sampler[args.gpu_id], seed=0,
        transform=torchvision.transforms.Compose([
            torchvision.transforms.ToTensor(),
            torchvision.transforms.Resize(size=(224,)*2),
        ]),
    )

    dataloader = torch.utils.data.DataLoader(dataset, shuffle=False, batch_size=4)

    metric = metrics.CorrectCount(threshold=0.85)
    similarity_list = []
    correct_count = engine.pipelines.evaluate_model(
        model=model, dataloader=dataloader, device=device, logger=logger, metric=metric, similarity_list=similarity_list,
    )

      info_json = {
        "acc": float(correct_count/len(dataset))
    }
    print(f"acc={correct_count/len(dataset)}")
   
    try: 
        # path_noext = f"plots/DINO/dino1"
        path_noext = f"plots/{dino_model_name[args.model_id]}_{args.model_id}/score_distribution_{args.gpu_id}_{gpu2sampler[args.gpu_id]}_{args.model_id}_{dino_model_name[args.model_id]}"
        torch.save(torch.Tensor(similarity_list), path_noext+".pt")
        plt.figure()
        plt.hist(similarity_list, bins=50)
        # print( "==================name===================")
        # print(str(gpu2sampler[args.gpu_id]))
        plt.title(f"DINO_{args.gpu_id} [{str(gpu2sampler[args.gpu_id])}] [{dino_model_name[args.model_id]}]")
        # plt.title(f"DINO1")
        plt.savefig(path_noext+".png")
        with open(f"{path_noext}.txt", "w") as txtfile:
            txtfile.write(json.dumps(info_json))
    except Exception as e:
        print(e)
        print("got error in storing......")
        path_noext = f"plots/{dino_model_name[args.model_id]}_{args.model_id}/output"
        torch.save(torch.Tensor(similarity_list), path_noext+".pt")
        plt.figure()
        plt.hist(similarity_list, bins=50)
        # print( "==================name===================")
        # print(str(gpu2sampler[args.gpu_id]))
        plt.title(f"DINO_{args.gpu_id} output ")
        # plt.title(f"DINO1")
        plt.savefig(path_noext+".png")
        with open(f"{path_noext}.txt", "w") as txtfile:
            txtfile.write(json.dumps(info_json))

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gpu_id', type=int, default=3,
    )
    parser.add_argument(
        '--model_id', type=int, default=0,
    )
    args = parser.parse_args()
    main(args)
