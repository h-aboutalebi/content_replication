import numpy
import torch
import torchvision
from tqdm import tqdm
from PIL import Image
import matplotlib.pyplot as plt
import os
import glob

import sys
sys.path.append("..")
from engine.datasets.style_transfer.ControlNet import Model


save_dir = "/home/dani/repos/content_replication/scripts_style_transfer"
prompt = "two aliens"
a_prompt = "high quality, realistic"
n_prompt = "longbody, lowres, bad anatomy, bad hands, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality"
image_paths = [
    "/home/dani/repos/content_replication/scripts_style_transfer/exp_saina.jpg"
]
model = Model(gpu_id=7)
for idx, path in tqdm(enumerate(image_paths)):
    image = numpy.array(Image.open(path))
    assert len(image.shape) == 3 and image.shape[-1] == 3 and image.dtype == numpy.uint8 and 0 <= numpy.min(image) <= numpy.max(image) <= 255, f"{image=}"
    results = model.process_canny(
        image=image,
        prompt=prompt,
        additional_prompt=a_prompt,
        negative_prompt=n_prompt,
        num_images=1,
        image_resolution=1024,
        num_steps=20,
        guidance_scale=30,
        seed=0,
        low_threshold=150,
        high_threshold=200,
    )
    assert len(results) == 2, f"{len(results)=}"
    results[0] = results[0].resize((image.shape[1], image.shape[0]))
    results[1] = results[1].resize((image.shape[1], image.shape[0]))
    results[0] = numpy.array(results[0])
    results[1] = numpy.array(results[1])
    assert type(image) == type(results[0]) == type(results[1]) == numpy.ndarray
    assert image.shape[:2] == results[0].shape[:2] == results[1].shape[:2], \
        f"{image.shape=}, {results[0].shape=}, {results[1].shape=}"
    Image.fromarray(image).save(os.path.join(save_dir, f"{idx:03d}_original.png"))
    Image.fromarray(results[0]).save(os.path.join(save_dir, f"{idx:03d}_edges.png"))
    Image.fromarray(results[1]).save(os.path.join(save_dir, f"{idx:03d}_generated.png"))
