import os
import glob
from tqdm import tqdm
import sys
sys.path.append("..")

import engine
from engine.datasets.style_transfer.ControlNet import HQ_512
from engine.datasets.style_transfer.ControlNet import HQ_1024
from engine.datasets.style_transfer.ControlNet import HQ_discard
from engine.datasets.style_transfer.ControlNet import HQ_set
from engine.datasets.style_transfer.ControlNet import HQ_more_1024


split_options = engine.datasets.source.WikiArt.split_options
style_transfer_root = "/home/dani/repos/content_replication/DeepfakeArt/similar/style_transfer"


def sanity_check():
    print("Performing sanity check...")
    assert set(HQ_512.keys()) == set(HQ_1024.keys()) == set(HQ_set.keys()) == set(HQ_discard.keys()) == set(split_options)
    for split in split_options:
        assert len(set(HQ_512[split]) & set(HQ_1024[split])) == 0
        assert len(set(HQ_512[split]) & set(HQ_discard[split])) == 0
        assert len(set(HQ_1024[split]) & set(HQ_discard[split])) == 0
        union = set(HQ_512[split]) | set(HQ_1024[split]) | set(HQ_discard[split])
        assert union == set(HQ_set[split]), f"{split=}, {union^set(HQ_set[split])=}"


def collect_first_200():
    print("Collecting from first 200 results...")
    for split in tqdm(split_options, leave=False):
        for idx in tqdm(range(200), leave=False):
            if idx in HQ_512[split]:  # use 512
                for postfix in ["original", "edges", "generated"]:
                    source_path = f"/home/dani/datasets/style_transfer_512_full_results/{split}/{idx:03d}_{postfix}.png"
                    target_path = os.path.join(style_transfer_root, f"{split}/{idx:03d}_{postfix}.png")
                    os.system(f"cp {source_path} {target_path}")
            elif idx in HQ_1024[split]:  # use 1024
                for postfix in ["original", "edges", "generated"]:
                    source_path = f"/home/dani/datasets/style_transfer_1024_full_results/{split}/{idx:03d}_{postfix}.png"
                    target_path = os.path.join(style_transfer_root, f"{split}/{idx:03d}_{postfix}.png")
                    os.system(f"cp {source_path} {target_path}")
            else:  # don't use this index
                assert idx not in HQ_set[split] or idx in HQ_discard[split]
                for postfix in ["original", "edges", "generated"]:
                    target_path = os.path.join(style_transfer_root, f"{split}/{idx:03d}_{postfix}.png")
                    if os.path.exists(target_path):
                        os.system(f"rm {target_path}")


def collect_second_200():
    print("Collecting from second 200 results...")
    for split in tqdm(split_options, leave=False):
        for idx in tqdm(range(200, 400), leave=False):
            if idx in HQ_more_1024[split]:
                for postfix in ["original", "edges", "generated"]:
                    source_path = f"/home/dani/datasets/style_transfer_ControlNet_more_1024/{split}/{idx:03d}_{postfix}.png"
                    target_path = os.path.join(style_transfer_root, f"{split}/{idx:03d}_{postfix}.png")
                    os.system(f"cp {source_path} {target_path}")
            else:
                for postfix in ["original", "edges", "generated"]:
                    target_path = os.path.join(style_transfer_root, f"{split}/{idx:03d}_{postfix}.png")
                    if os.path.exists(target_path):
                        os.system(f"rm {target_path}")


def verify_updates():
    print("Verifying updates...")
    # get indices from configs
    idx_configs = dict(
        (split, HQ_512[split] | HQ_1024[split] | HQ_more_1024[split])
        for split in split_options
    )
    # get indices from results
    idx_results = dict(
        (split, set(map(lambda x: int(x.split('/')[-1].split('_')[0]), list(glob.glob(os.path.join(style_transfer_root, f"{split}/*.png"))))))
        for split in split_options
    )
    # check
    for split in split_options:
        assert idx_configs[split] == idx_results[split], f"{split=}\n{idx_configs[split]=}\n{idx_results[split]=}\n{idx_configs[split]^idx_results[split]=}\n"


if __name__ == "__main__":
    sanity_check()
    collect_first_200()
    collect_second_200()
    verify_updates()
