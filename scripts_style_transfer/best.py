import os


best = {
    "realistic": [
        ("New_Realism", 21),
        ("New_Realism", 46),
        ("Romanticism", 42),
        ("Synthetic_Cubism", 27),
        ("Synthetic_Cubism", 36),
        ("Symbolism", 41),
        ("Symbolism", 44),
    ],
    "cartoon": [
        ("Art_Nouveau_Modern", 54),
        ("Baroque", 90),
        ("Cubism", 69),

    ],
    "oil": [
        ("Naive_Art_Primitivism", 117),
        ("Naive_Art_Primitivism", 127),
        ("New_Realism", 137),
        ("Synthetic_Cubism", 129),
        ("Ukiyo_e", 117),
    ],
    "pencil": [
        ("Symbolism", 192),
        ("Synthetic_Cubism", 167),
        ("Ukiyo_e", 151),
    ],
}

src_dir_512 = "/home/dani/datasets/style_transfer_512_full_results"
src_dir_1024 = "/home/dani/datasets/style_transfer_1024_full_results"

for key, val in best.items():
    dst_dir = f"../datasets_tmp/style_transfer_best/{key}"
    src_dir = src_dir_512 if key == "cartoon" else src_dir_1024
    for split, idx in val:
        for postfix in ["original", "generated"]:
            src_path = os.path.join(src_dir, f"{split}/{idx:03d}_{postfix}.png")
            dst_path = os.path.join(dst_dir, f"{split}_{idx:03d}_{postfix}.png")
            os.system(f"cp {src_path} {dst_path}")

