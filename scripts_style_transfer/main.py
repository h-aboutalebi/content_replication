import numpy
import torch
import torchvision
from tqdm import tqdm
from PIL import Image
import matplotlib.pyplot as plt
import os
import glob
import json
import argparse
import sys
sys.path.append("..")

from engine.datasets.style_transfer.ControlNet import Model
import engine.datasets.source.WikiArt.WikiArtDataset as WikiArtDataset

from engine.datasets.style_transfer.ControlNet import HQ_512
from engine.datasets.style_transfer.ControlNet import HQ_1024


def idx2prompt(idx, dataset_size):
    assert idx < dataset_size
    dataset_size = dataset_size - 200 * (idx // 200)
    idx = idx % 200
    num_per_prompt = min(50, dataset_size // 4)
    if idx < num_per_prompt:
        prompt = "a high-quality, detailed, realistic image"
    elif idx < 2 * num_per_prompt:
        prompt = "a high-quality, detailed, cartoon style drawing"
    elif idx < 3 * num_per_prompt:
        prompt = "a high-quality, detailed, oil painting"
    elif idx < 4 * num_per_prompt:
        prompt = "a high-quality, detailed, pencil drawing"
    else:
        prompt = None
    return prompt


def generate():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gpu_id', type=int, required=True, default=7,
    )
    args = parser.parse_args()
    model = Model(gpu_id=args.gpu_id)
    split_options = {
        7: [
            "Abstract_Expressionism",
            "Action_painting",
            "Analytical_Cubism",
            "Art_Nouveau_Modern",
            "Baroque",
            "Color_Field_Painting",
        ],
        6: [
            "Contemporary_Realism",
            "Cubism",
            "Early_Renaissance",
            "Expressionism",
            "Fauvism",
        ],
        5: [
            "High_Renaissance",
            "Impressionism",
            "Mannerism_Late_Renaissance",
            "Minimalism",
            "Naive_Art_Primitivism",
        ],
        4: [
            "New_Realism",
            "Northern_Renaissance",
            "Pointillism",
            "Pop_Art",
            "Post_Impressionism",  # OSError: broken data stream when reading image file
        ],
        3: [
            "Realism",
            "Rococo",
            "Romanticism",
        ],
        2: [
            "Symbolism",
            "Synthetic_Cubism",
            "Ukiyo_e",
        ],
    }
    save_dir = "/home/dani/datasets/style_transfer_1024_full_results"
    for split in split_options[args.gpu_id]:
        dataset = WikiArtDataset(split=split)
        torch.manual_seed(0)
        dataloader = torch.utils.data.DataLoader(dataset, shuffle=True)
        a_prompt = "best quality, extremely detailed"
        n_prompt = "longbody, lowres, bad anatomy, bad hands, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality"
        for idx, (image, path) in tqdm(enumerate(dataloader)):
            prompt = idx2prompt(idx, dataset_size=len(dataset))
            if prompt is None:
                break
            resolution = 1024
            assert 0 <= torch.min(image) <= torch.max(image) <= 1
            assert image.shape[0] == 1
            assert image.dtype == torch.float32
            image = image * 255
            image = image[0]
            image = torch.permute(image, dims=[1, 2, 0])
            image = image.numpy()
            image = image.astype(numpy.uint8)
            results = model.process_canny(
                image=image,
                prompt=prompt,
                additional_prompt=a_prompt,
                negative_prompt=n_prompt,
                num_images=1,
                image_resolution=resolution,
                num_steps=20,
                guidance_scale=9,
                seed=0,
                low_threshold=100,
                high_threshold=200,
            )
            assert len(results) == 2, f"{len(results)=}"
            results[0] = results[0].resize((image.shape[1], image.shape[0]))
            results[1] = results[1].resize((image.shape[1], image.shape[0]))
            results[0] = numpy.array(results[0])
            results[1] = numpy.array(results[1])
            assert type(image) == type(results[0]) == type(results[1]) == numpy.ndarray
            assert image.shape[:2] == results[0].shape[:2] == results[1].shape[:2], \
                f"{image.shape=}, {results[0].shape=}, {results[1].shape=}"
            Image.fromarray(image).save(os.path.join(save_dir, split, f"{idx:03d}_original.png"))
            Image.fromarray(results[0]).save(os.path.join(save_dir, split, f"{idx:03d}_edges.png"))
            Image.fromarray(results[1]).save(os.path.join(save_dir, split, f"{idx:03d}_generated.png"))


if __name__ == "__main__":
    generate()

