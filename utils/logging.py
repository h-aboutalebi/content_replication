import logging
import sys


def get_logger(filepath):
    logger = logging.getLogger()
    logger.setLevel(level=logging.INFO)
    formatter = logging.Formatter(
        fmt=f"[%(levelname)s] %(asctime)s - %(message)s",
        datefmt="%m-%d %H:%M:%S",
    )
    # A more complete version of formatter
    # formatter = logging.Formatter(
    #     fmt=f"%(levelname)s %(asctime)s (%(relativeCreated)d) %(pathname)s F%(funcName)s L%(lineno)s - %(message)s",
    #     datefmt="%Y-%m-%d %H:%M:%S",
    # )
    if filepath is not None:
        # if not os.path.exists(filepath):
            
        f_handler = logging.FileHandler(filename=filepath)
        f_handler.setFormatter(formatter)
        f_handler.setLevel(level=logging.INFO)
        logger.addHandler(f_handler)
    s_handler = logging.StreamHandler(stream=sys.stdout)
    s_handler.setFormatter(formatter)
    s_handler.setLevel(level=logging.INFO)
    logger.addHandler(s_handler)
    return logger
