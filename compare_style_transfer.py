import os
import glob
from tqdm import tqdm
from PIL import Image, ImageFont, ImageDraw


new_root_dir = "/home/dani/repos/content_replication/dataset_tmp/style_transfer_ControlNet"
old_root_dir = "/home/dani/repos/content_replication/DeepfakeArt/style_transfer_ControlNet"
original_images = sorted(
    glob.glob(os.path.join(new_root_dir, "**/*_original.png")),
)
old_generated_images = sorted(
    glob.glob(os.path.join(old_root_dir, "**/*_generated.png")),
)
new_generated_images = sorted(
    glob.glob(os.path.join(new_root_dir, "**/*_generated.png")),
)
print(f"Total count: {len(original_images)}.")
for (original_path, old_gen_path, new_gen_path) in tqdm(zip(original_images, old_generated_images, new_generated_images)):
    assert original_path.split('/')[-2] == old_gen_path.split('/')[-2] == new_gen_path.split('/')[-2]
    split = original_path.split('/')[-2]
    assert int(original_path.split('/')[-1].split('_')[0]) == int(old_gen_path.split('/')[-1].split('_')[0]) == int(new_gen_path.split('/')[-1].split('_')[0])
    idx = int(original_path.split('/')[-1].split('_')[0])
    original_image = Image.open(original_path)
    old_gen_image = Image.open(old_gen_path)
    new_gen_image = Image.open(new_gen_path)
    assert original_image.size == old_gen_image.size == new_gen_image.size
    width, height = original_image.size
    width /= 1.5
    height /= 1.5
    # define comparison image
    new_width = 3 * width
    new_height = height + 40
    compare = Image.new("RGB", (new_width, new_height), color=(255, 255, 255))
    compare.paste(original_image.resize(size=(width, height)), (0, 0))
    compare.paste(old_gen_image.resize(size=(width, height)), (width, 0))
    compare.paste(new_gen_image.resize(size=(width, height)), (2*width, 0))
    # add title
    font = ImageFont.truetype("arial.ttf", size=30)
    draw = ImageDraw.Draw(compare)
    original_text = "Original Image"
    old_text = "Old Style Transfer"
    new_text = "New Style Transfer"
    original_text_width = draw.textlength(text=original_text, font=font)
    old_text_width = draw.textlength(text=old_text, font=font)
    new_text_width = draw.textlength(text=new_text, font=font)
    draw.text(((width-original_text_width)//2, height), original_text, font=font, fill=(0, 0, 0))
    draw.text((width+(width-old_text_width)//2, height), old_text, font=font, fill=(0, 0, 0))
    draw.text((2*width+(width-new_text_width)//2, height), new_text, font=font, fill=(0, 0, 0))
    compare.save(f"style_transfer_compare/{split}/{idx:03d}_compare.png")
