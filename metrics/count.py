import torch


class Metric:
    def __init__(self, threshold=0.85):
        self.threshold = threshold

    def run(self, similarity, ground_truth, prediction_truth):
        assert similarity.shape == ground_truth.shape
        predictions = similarity > self.threshold
        correct_count = torch.sum(torch.eq(predictions, ground_truth) & torch.eq(predictions, prediction_truth))
        return correct_count 

class TPCount(Metric):
    def __init__(self, threshold=0.85):
        super().__init__(threshold)

    def __call__(self, similarity, ground_truth):
        return super().run(similarity, ground_truth, True)

    def metric_name(self):
        return "TP"
    
class TNCount(Metric):
    def __init__(self, threshold=0.85):
        super().__init__(threshold)

    def __call__(self, similarity, ground_truth):
        return super().run(similarity, ground_truth, False)

    def metric_name(self):
        return "TN"
    
class FPCount(Metric):
    def __init__(self, threshold=0.85):
        super().__init__(threshold)

    def __call__(self, similarity, ground_truth):
        return super().run(similarity, ~ground_truth, True)

    def metric_name(self):
        return "FP"

class FNCount(Metric):
    def __init__(self, threshold=0.85):
        super().__init__(threshold)

    def __call__(self, similarity, ground_truth):
        return super().run(similarity, ~ground_truth, False)
    
    def metric_name(self):
        return "FN"