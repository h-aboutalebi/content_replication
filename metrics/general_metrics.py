

class GeneralMetrics:
    def __init__(self, metrics_dict: dict) -> None:
        self.TP = metrics_dict['TP']
        self.TN = metrics_dict['TN']
        self.FP = metrics_dict['FP']
        self.FN = metrics_dict['FN']
        self.precision = -2 # default
        self.recall = -2 # default

    def get_accuracy(self):
        N = self.TP + self.TN + self.FP + self.FN 
        return (self.TP + self.TN) / N if N != 0 else -1
    
    def get_precision(self):
        self.precision = self.TP / (self.TP + self.FP) if self.TP + self.FP != 0 else -1

        return self.precision

    def get_recall(self):
        self.recall = self.TP / (self.TP + self.FN) if self.TP + self.FN != 0 else -1

        return self.recall

    def get_f1(self):
        if self.recall == -2:
            self.recall = self.get_recall()
        if self.precision == -2:
            self.precision = self.get_precision()

        if self.recall == -1 or self.precision == -1:
            self.f1 = -1
        else:
            self.f1 = 2 * self.precision * self.recall / (self.precision + self.recall)

        return self.f1
    
    def __str__(self):
        return f"TP: {self.TP}; TN: {self.TN}; FP: {self.FP}; FN: {self.FN};"