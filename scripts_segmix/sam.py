import numpy
import torch
from PIL import Image, ImageFont, ImageDraw
import os
from tqdm import tqdm
import time
import argparse

import engine


def get_display(mask):
    color_list = [
        (255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255),
        (127, 0, 0), (0, 127, 0), (0, 0, 127), (127, 127, 0), (127, 0, 127), (0, 127, 127),
        (191, 63, 0), (191, 0, 63), (63, 191, 0), (0, 191, 63), (63, 0, 191), (0, 63, 191),
    ]
    display_mask = numpy.zeros(shape=mask.shape+(3,))
    obj_list = list(set(numpy.unique(mask)) - set([0]))
    for idx, obj in enumerate(obj_list[:18]):
        display_mask[..., 0][mask == obj] = color_list[idx][0]
        display_mask[..., 1][mask == obj] = color_list[idx][1]
        display_mask[..., 2][mask == obj] = color_list[idx][2]
    display_mask = display_mask.astype(numpy.uint8)
    return display_mask


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--gpu_id', type=int, required=True,
    )
    args = parser.parse_args()
    begin_mapping = {
        7: 0,
        6: 500,
        5: 1000,
        4: 1500,
        3: 2000,
        2: 2500,
    }
    end_mapping = {
        7: 500,
        6: 1000,
        5: 1500,
        4: 2000,
        3: 2500,
        2: 3000,
    }
    save_root = "/home/dani/datasets/WikiArtSegmentation"
    model = engine.segment_anything.SAM(device=f"cuda:{args.gpu_id}")
    all_paths = engine.datasets.source.get_source_dataset(source_name="StyleTransfer3k").all_paths[begin_mapping[args.gpu_id]:end_mapping[args.gpu_id]]
    print(f"Generating masks in range {begin_mapping[args.gpu_id]}:{end_mapping[args.gpu_id]}")
    for idx, img_path in tqdm(enumerate(all_paths), leave=False):
        try:
            mask = model(img_path)
            split = img_path.split('/')[-2]
            img_name = img_path.split('/')[-1]
            assert img_name.split('.')[-1] == "jpg", f"{img_name=}"
            # save tensor
            numpy.save(arr=mask, file=os.path.join(save_root, split, img_name.split('.')[0]+".npy"))
            # create demo
            height, width = mask.shape[0], mask.shape[1]
            display_mask = get_display(mask)
            compare = Image.new("RGB", (2*width+30, height+50), color=(255, 255, 255))
            compare.paste(Image.open(img_path), (0, 0))
            compare.paste(Image.fromarray(display_mask), (width+30, 0))
            font = ImageFont.truetype("arial.ttf", size=30)
            draw = ImageDraw.Draw(compare)
            text = f"num objects = {len(set(numpy.unique(mask)) - set([0]))}"
            text_width = draw.textlength(text=text, font=font)
            draw.text(((2*width+30-text_width)//2, height+10), text, font=font, fill=(0, 0, 0))
            compare.save(os.path.join(save_root, split, img_name.split('.')[0]+"_demo.png"))
        except torch.cuda.OutOfMemoryError:
            with open("OOM_idx.txt", mode='a') as f:
                f.write(f"torch.cuda.OutOfMemoryError at {img_path=}\n")
    print("Done.")
