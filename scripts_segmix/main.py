import sys
sys.path.append("..")
import engine


source_dataset = engine.datasets.source.WikiArt.WikiArtSegmentation()
generator = engine.datasets.segmix.SegmixGen(
    source=source_dataset,
    config_path="/home/dani/repos/content_replication/engine/datasets/segmix/config_Segmix_WikiArtSegmentation.json",
    images_dir="/home/dani/repos/content_replication/datasets_tmp/segmix",
)
generator.generate_config(num_examples=5000)
generator.generate_images()
